#!/bin/bash

# TODO: Move auth to some sort of secret keeping mechianism.
tv="pyvizio --ip=viziotv.lan:7345 --auth=Zu22jm2dno" 

pyvizio_with_retries() {
  counter=0
  until [[ $counter -ge 3 ]]
  do
    "$@" 2>&1 | grep -A10 -B 10 "OK" && break
    counter=$(($counter+1))
  done
}

if [[ "$1" == "night" ]]
then
  pyvizio_with_retries $tv setting picture picture_mode "Calibrated Dark*"
elif [[ "$1" == "game" ]]
then
  pyvizio_with_retries $tv setting picture picture_mode "Game*"
else
  pyvizio_with_retries $tv setting picture picture_mode "Calibrated*"
fi
