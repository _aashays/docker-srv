#!/usr/bin/python3

import json
import sys

import libdyson

_HOSTNAME = 'bedroomfan.lan'

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit(1)
    with open('dyson/creds.json') as c:
        creds = json.load(c)[_HOSTNAME]

    fan = libdyson.DysonPureCoolLink(creds['serial'],
            creds['credential'], creds['device_type'])
    fan.connect(_HOSTNAME)

    if sys.argv[1] == 'on':
        for _ in range(3):
            fan.turn_on()
            fan.enable_night_mode()
            fan.set_speed(7)

    elif sys.argv[1] == 'off':
        for _ in range(3):
            fan.turn_off()

    else:
        sys.exit(1)
