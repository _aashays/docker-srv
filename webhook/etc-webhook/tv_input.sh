#!/bin/bash

# TODO: Move auth to some sort of secret keeping mechianism.
tv="pyvizio --ip=viziotv.lan:7345 --auth=Zu22jm2dno" 
soundbar="pyvizio --ip=viziosoundbar.lan:9000 --device_type=speaker"

pyvizio_with_retries() {
  counter=0
  until [[ $counter -ge 3 ]]
  do
    $@ 2>&1 | grep -A10 -B 10 "OK" && break
    counter=$(($counter+1))
  done
}

if [[ "$1" == "shield" ]]
then
  pyvizio_with_retries $soundbar input HDMI
  $tv input HDMI-1
elif [[ "$1" == "playstation" ]]
then
  pyvizio_with_retries $soundbar input HDMI-ARC
  $tv input HDMI-2
elif [[ "$1" == "switch" ]]
then
  pyvizio_with_retries $soundbar input HDMI-ARC
  $tv input HDMI-3
elif [[ "$1" == "chromecast" ]]
then
  pyvizio_with_retries $soundbar input HDMI-ARC
  $tv input HDMI-4
fi
