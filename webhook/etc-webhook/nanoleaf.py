#!/usr/bin/python3

import sys
from difflib import get_close_matches

import auri
from auri.aurora import Aurora
from auri.device_manager import DeviceManager

if __name__ == "__main__":
  if len(sys.argv) != 2:
    sys.exit(1)
  if '-' in sys.argv[1]:
    effect, brightness = sys.argv[1].strip().split('-')
  else:
     effect = sys.argv[1]
     brightness = 0
  brightness = int(brightness)
  dm = DeviceManager()
  a = dm.get_by_name_or_active(None)
  if effect == 'off':
      a.on=False
      sys.exit(0)
  if effect == 'on':
      a.on=True
      a.brightness = brightness
      sys.exit(0)
  closest = get_close_matches(effect, a.get_effect_names(), n=1, cutoff=0)
  if not closest:
    sys.exit(1)
  a.set_active_effect(closest[0])
  if brightness:
    a.brightness = brightness
