#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
pushd $DIR

sudo git secret reveal -f

M=wireguard
grep $M /etc/modules || echo $M | sudo tee /etc/modules && sudo modprobe $M

uid=$(grep dockremap /etc/subuid | cut -d':' -f2)
gid=$(grep dockremap /etc/subgid | cut -d':' -f2)
sudo chown -R $uid:$gid */etc-*

for d in $(ls -d */); do
  docker compose -f /srv/docker-srv/$d/docker-compose.yml pull
  docker compose -f /srv/docker-srv/$d/docker-compose.yml up --force-recreate -d
done

popd
